# -*- coding: utf-8 -*-
from django.contrib import admin
from games.models import *


admin.site.register(ImageCategory)
admin.site.register(GameRecognition)
admin.site.register(CountingGame)
admin.site.register(RecognitionImage)
