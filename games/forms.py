import random
import string
from django import forms
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from games.models import LEVEL_CHOICES
from users.models import BaseUser, Teacher, Parent


class GameRecognitionForm(forms.Form):
    answer = forms.ChoiceField(label="Answer", required=False)

    def __init__(self, *args, **kwargs):
        self.image = kwargs.pop("image")
        self.choices= kwargs.pop("choices",None)
        self.answer= kwargs.pop("answer",None)

        super(GameRecognitionForm, self).__init__(*args, **kwargs)
        choices = []
        if self.choices:
            for choice in self.choices:
                choices.append( (choice,choice) )
            self.fields['answer'].widget = forms.RadioSelect(choices=choices)

            #self.fields['answer'].choices = choices
        #self.fields['date_from'].widget = forms.SplitDateTimeField

    # def clean(self):
    #     cleaned_data = self.cleaned_data
    #     return cleaned_data

    def is_correct(self):
        if self.answer == self.image.name:
            return True
        else:
            return False
