# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ImageCategory'
        db.create_table(u'games_imagecategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
        ))
        db.send_create_signal(u'games', ['ImageCategory'])

        # Adding model 'GameRecognition'
        db.create_table(u'games_gamerecognition', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('level', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('rounds', self.gf('django.db.models.fields.IntegerField')(default=3)),
            ('category_of_images', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['games.ImageCategory'])),
            ('images_per_round', self.gf('django.db.models.fields.IntegerField')(default=3)),
        ))
        db.send_create_signal(u'games', ['GameRecognition'])

        # Adding model 'RecognitionImage'
        db.create_table(u'games_recognitionimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['games.ImageCategory'])),
        ))
        db.send_create_signal(u'games', ['RecognitionImage'])

        # Adding model 'GameAnswer'
        db.create_table(u'games_gameanswer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['games.GameRecognition'])),
            ('player', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.BaseUser'])),
            ('points', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'games', ['GameAnswer'])


    def backwards(self, orm):
        # Deleting model 'ImageCategory'
        db.delete_table(u'games_imagecategory')

        # Deleting model 'GameRecognition'
        db.delete_table(u'games_gamerecognition')

        # Deleting model 'RecognitionImage'
        db.delete_table(u'games_recognitionimage')

        # Deleting model 'GameAnswer'
        db.delete_table(u'games_gameanswer')


    models = {
        u'games.gameanswer': {
            'Meta': {'object_name': 'GameAnswer'},
            'game': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['games.GameRecognition']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'player': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.BaseUser']"}),
            'points': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'games.gamerecognition': {
            'Meta': {'object_name': 'GameRecognition'},
            'category_of_images': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['games.ImageCategory']"}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'images_per_round': ('django.db.models.fields.IntegerField', [], {'default': '3'}),
            'level': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'rounds': ('django.db.models.fields.IntegerField', [], {'default': '3'})
        },
        u'games.imagecategory': {
            'Meta': {'object_name': 'ImageCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'games.recognitionimage': {
            'Meta': {'object_name': 'RecognitionImage'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['games.ImageCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        },
        u'users.baseuser': {
            'Meta': {'object_name': 'BaseUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['games']