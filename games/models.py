from django.db import models
from users.models import BaseUser

from taggit.managers import TaggableManager

LEVEL_CHOICES = (
    ("easy", "Easy"),
    ("medium", "Medium"),
    ("hard", "Hard"),
)
# Create your models here.
class ImageCategory(models.Model):
    name = models.CharField(verbose_name="Name", max_length=250)
    tags = TaggableManager()

    class Meta:
        verbose_name = "Image category"
        verbose_name_plural = "Images categories"

    def __unicode__(self):
        return self.name

class BaseGame(models.Model):
    name = models.CharField(verbose_name="Name", max_length=250)
    description = models.TextField(verbose_name="Description", null=True, blank=True)
    level = models.CharField(verbose_name="Level", choices=LEVEL_CHOICES, max_length=50 )
    rounds = models.IntegerField(verbose_name="Rounds", default=3)
    tags = TaggableManager()

    class Meta:
        verbose_name = "Base game"
        verbose_name_plural = "Bases games"


    def __unicode__(self):
        return self.name

    def get_counting_instance(self):
        try:
            counting = CountingGame.objects.get(id=self.id)
        except:
            counting = None
        return counting

    def is_counting_game(self):
        if self.get_counting_instance():
            return True
        else:
            return False


class GameRecognition(BaseGame):
    category_of_images = models.ForeignKey(ImageCategory, verbose_name="Image category", help_text="Random images from this category")
    images_per_round = models.IntegerField(verbose_name="Images per round", default=3)

    class Meta:
        verbose_name = "Game recognition setting"
        verbose_name_plural = "Games recognitions settings"

class CountingGame(BaseGame):
    max_number = models.IntegerField(verbose_name="Max randomed number")
    allowed_symbols = models.CharField(verbose_name="Allowed symbols", default="*,/,+,-", max_length=20)

    class Meta:
        verbose_name = "Counting game"
        verbose_name_plural = "Counting games"


class RecognitionImage(models.Model):
    image = models.ImageField(verbose_name="Image", upload_to="media/game_images/")
    name = models.CharField(verbose_name="Name", max_length=250)
    category = models.ForeignKey(ImageCategory, verbose_name="Image category")

    class Meta:
        verbose_name = "Recognition image"
        verbose_name_plural = "Recognitions images"

    def __unicode__(self):
        return self.name

class GameAnswer(models.Model):
    game = models.ForeignKey(BaseGame, verbose_name="Game")
    player = models.ForeignKey(BaseUser, verbose_name="Player")
    points = models.IntegerField(verbose_name="Points", default=0)
    randomed_images = models.ManyToManyField(RecognitionImage, null=True,blank=True)

    class Meta:
        verbose_name = "Game answer"
        verbose_name = "Games answers"

    def __unicode__(self):
        return "%s Points: %s" % (self.player, self.points)
