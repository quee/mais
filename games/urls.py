from django.conf.urls import patterns, include, url
from django.contrib.messages.context_processors import messages
from games.views import show_games, choose_game, game_finished, show_counting_game, search_games
from messages.views import show_messages, compose_message, show_message
from users.forms import ParentRegistrationForm
from users.views import registration
from django.contrib.auth import views as auth_views

urlpatterns = patterns('',
    # Examples:'
    url(r'^$', show_games, name='show_games'),
    url(r'^search/$', search_games, name='search_games'),

    url(r'^finished/(?P<id>[\d]+)/$', game_finished, name='game_finished'),

    url(r'^game/(?P<id>[\d]+)/$', choose_game, name='choose_game'),
    url(r'^game/(?P<id>[\d]+)/(?P<game_id>[\d]+)/(?P<image_id>[\d]+)/(?P<game_round>[\d]+)/$', choose_game, name='choose_game'),

    url(r'^game/counting/(?P<id>[\d]+)/$', show_counting_game, name='show_counting_game'),
    url(r'^game/counting/(?P<id>[\d]+)/(?P<game_id>[\d]+)/(?P<game_round>[\d]+)/$', show_counting_game, name='show_counting_game'),

    # url(r'^compose/$', compose_message, name='compose_message'),
    # url(r'^message/(?P<id>[\d]+)/$', show_message, name='show_message'),



)
