import random
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from games.forms import GameRecognitionForm
from games.models import GameRecognition, RecognitionImage, GameAnswer, CountingGame, BaseGame


@login_required
def show_games(request):
    template = "games/show_all_games.html"
    games = BaseGame.objects.all()

    data = {'games': games}
    return render_to_response(template, data, context_instance=RequestContext(request))

@login_required
def search_games(request):
    template = "games/show_all_games.html"
    search = request.POST.get('search',None)
    print search
    games = BaseGame.objects.filter((Q(name=search) | Q(description=search)))

    data = {'games': games}
    return render_to_response(template, data, context_instance=RequestContext(request))

@login_required
def show_counting_game(request, id=None, game_id=None, game_round = None ):
    template = "games/game_counting.html"
    game = CountingGame.objects.get(id=id)
    if not game_id:
        player_game = GameAnswer.objects.create(game=game, player=request.user)
    else:
        player_game = GameAnswer.objects.get(id=game_id)

    if not game_round:
        game_round = 1
    else:
        game_round = int(game_round) + 1

    if request.POST:
        answer = request.POST.get('answer',None)
        text_answer = request.POST.get('text_answer',None)
        print answer,text_answer,  answer == text_answer
        if answer == text_answer:
            player_game.points = player_game.points + 1
            player_game.save()
        else:
            player_game.points = player_game.points - 1
            player_game.save()

    if(game_round > game.rounds):
        return redirect( reverse("game_finished",args=[player_game.id]))


    sign = random.choice( game.allowed_symbols.split(',') )
    number1 = random.randint(0, game.max_number)
    number2 = random.randint(0, game.max_number)
    answer = 0
    if sign == "+":
        answer = number1 + number2
    elif sign == "-":
        answer = number1 - number2
    elif sign == "*":
        answer = number1 * number2
    elif sign == "/":
        number1 = number1 * number2
        answer = number1 / number2

    formula = "%s %s %s = ?" % (number1,sign,number2)


    data = {'game': game,'player_game':player_game, 'game_round':game_round, 'answer':answer,'formula':formula}
    return render_to_response(template, data, context_instance=RequestContext(request))


@login_required
def choose_game(request, id=None, game_id=None, image_id=None, game_round = None):
    template = "games/game.html"
    game = GameRecognition.objects.get(id=id)
    if not game_id:
        player_game = GameAnswer.objects.create(game=game, player=request.user)
    else:
        player_game = GameAnswer.objects.get(id=game_id)

    if not game_round:
        game_round = 1
    else:
        game_round = int(game_round) + 1


    if request.POST:
        answer_image = RecognitionImage.objects.get(id=image_id)
        player_game.randomed_images.add(answer_image)
        player_game.save()
        answer = request.POST.get('answer',None)
        last_form = GameRecognitionForm(request.POST,image=answer_image,answer=answer)
        last_form.is_valid()
        if last_form.is_correct():
            player_game.points = player_game.points + 1
            player_game.save()
        else:
            player_game.points = player_game.points - 1
            player_game.save()

    if(game_round > game.rounds):
        return redirect( reverse("game_finished",args=[player_game.id]))

    randomed_images = player_game.randomed_images.all().values_list('id',flat=True)
    randomed_images = list(randomed_images)
    if image_id:
        randomed_images.append(image_id)

    if randomed_images:
        image = RecognitionImage.objects.filter(category=game.category_of_images).exclude(id__in=randomed_images).order_by('?')[0]
    else:
        image = RecognitionImage.objects.filter(category=game.category_of_images).order_by('?')[0]

    answers = RecognitionImage.objects.filter(category=game.category_of_images).exclude(id=image.id).order_by('?')
    answers = answers[:game.images_per_round-1].values_list('name',flat=True)
    answers = list(answers) + [image.name,]
    form = GameRecognitionForm(image=image,choices=sorted(answers))




    data = {'game': game,'image':image,'form':form,'player_game':player_game, 'game_round':game_round}
    return render_to_response(template, data, context_instance=RequestContext(request))

def game_finished(request,id):
    template = "games/game_finished.html"
    player_game = GameAnswer.objects.get(id=id)
    game = player_game.game
    data = {'game': game, 'player_game':player_game}
    return render_to_response(template, data, context_instance=RequestContext(request))