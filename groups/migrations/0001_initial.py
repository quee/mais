# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ChildrenGroup'
        db.create_table(u'groups_childrengroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'groups', ['ChildrenGroup'])

        # Adding M2M table for field teachers on 'ChildrenGroup'
        db.create_table(u'groups_childrengroup_teachers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('childrengroup', models.ForeignKey(orm[u'groups.childrengroup'], null=False)),
            ('teacher', models.ForeignKey(orm[u'users.teacher'], null=False))
        ))
        db.create_unique(u'groups_childrengroup_teachers', ['childrengroup_id', 'teacher_id'])


    def backwards(self, orm):
        # Deleting model 'ChildrenGroup'
        db.delete_table(u'groups_childrengroup')

        # Removing M2M table for field teachers on 'ChildrenGroup'
        db.delete_table('groups_childrengroup_teachers')


    models = {
        u'groups.childrengroup': {
            'Meta': {'object_name': 'ChildrenGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'teachers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['users.Teacher']", 'null': 'True', 'blank': 'True'})
        },
        u'users.baseuser': {
            'Meta': {'object_name': 'BaseUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'users.teacher': {
            'Meta': {'object_name': 'Teacher', '_ormbases': [u'users.BaseUser']},
            u'baseuser_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['users.BaseUser']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['groups']