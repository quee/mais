from django.db import models
from django.utils.translation import ugettext as _

# Create your models here.

class ChildrenGroup(models.Model):
    name = models.CharField(verbose_name=_("Name"), max_length=200)
    teachers = models.ManyToManyField('users.Teacher',verbose_name=_("Teachers"), null=True, blank=True)


    def __unicode__(self):
        return self.name

    def children_number(self):
        return self.child_set.all().count()

    def get_children(self):
        return self.child_set.all()

