import random
import string
from django import forms
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from materials.models import Material
from users.models import BaseUser, Teacher, Parent


class MaterialForm(forms.ModelForm):
    pass

    class Meta:
        model = Material
        exclude = ['uploaded_by',]


    def save(self,uploaded_by=None, commit=True):
        m = super(MaterialForm, self).save(commit=False)
        m.uploaded_by = uploaded_by
        if commit:
            m.save()
        return m


