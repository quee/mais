from django.db import models

# Create your models here.
from users.models import BaseUser
from taggit.managers import TaggableManager


class Material(models.Model):
    name = models.CharField(verbose_name=u"Name", max_length=200)
    description = models.TextField(verbose_name=u"Description", null=True, blank=True)
    uploaded_by = models.ForeignKey(BaseUser,verbose_name=u"Uploaded by")
    file = models.FileField(verbose_name="File", upload_to="materials/")
    tags = TaggableManager()

    class Meta:
        verbose_name = "Material"
        verbose_name_plural = "Materials"

    def __unicode__(self):
        return self.name

    def get_file_url(self):
        return self.file.url

