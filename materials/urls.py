from django.conf.urls import patterns, include, url
from materials.views import materials, upload_material

from django.contrib.auth import views as auth_views

urlpatterns = patterns('',
    # Examples:'
    url(r'^$', materials, name='materials'),
    url(r'^upload/$', upload_material, name='upload_material'),
)
