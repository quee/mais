# Create your views here.
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from materials.forms import MaterialForm
from materials.models import Material


def materials(request):
    template = "materials/materials.html"
    materials = Material.objects.all()
    data = {'materials':materials}
    return render_to_response(template, data, context_instance=RequestContext(request))


def upload_material(request):
    template = "materials/upload_material.html"
    form = MaterialForm()
    if request.POST:
        form = MaterialForm(request.POST,request.FILES)
        if form.is_valid():
            form.save(uploaded_by=request.user)
            return redirect(reverse("materials"))

    data = {'form':form}
    return render_to_response(template, data, context_instance=RequestContext(request))
