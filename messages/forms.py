from django import forms
from messages.models import Message


class ComposeMessageForm(forms.ModelForm):

    class Meta:
        model = Message
        exclude = ['date','from_user','read','reply_to',]


    def save(self, from_user=None, commit=True):
        m = super(ComposeMessageForm, self).save(commit=False)
        m.from_user = from_user
        m.save()
