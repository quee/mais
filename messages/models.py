import datetime
from django.db import models
from users.models import BaseUser

class Message(models.Model):
    from_user = models.ForeignKey(BaseUser, verbose_name=u"From user",related_name="from_user")
    to_user = models.ForeignKey(BaseUser, verbose_name=u"To user",related_name="to_user")
    date = models.DateTimeField(verbose_name=u"Sending date", default=datetime.datetime.now())
    reply_to = models.ForeignKey('self',verbose_name=u"Reply to", null=True, blank=True)
    read = models.BooleanField(verbose_name=u"Read", default=False)
    subject = models.CharField(verbose_name=u"Subject", max_length=200)
    text = models.TextField(verbose_name=u"Text")

    class Meta:
        verbose_name = "Message" # Jak ma sie wyswietlac liczba pojedyczna obiektu
        verbose_name_plural = "Messages" # Jak ma sie wystalacz liczba mnoga obiektu (wykorzystywane w adminie)

    def __unicode__(self):
        """
        Funkcja ta zwraca nazwe tematu. Jezeli wywolujesz obiekt m = Message.objects.get(id=1) to wywolujac print m
        wywolujesz wlanie te funkcje automatycznie
        """
        return self.subject

