from django.conf.urls import patterns, include, url
from django.contrib.messages.context_processors import messages
from messages.views import show_messages, compose_message, show_message
from users.forms import ParentRegistrationForm
from users.views import registration
from django.contrib.auth import views as auth_views

urlpatterns = patterns('',
    # Examples:'
    url(r'^$', show_messages, name='show_messages'),
    url(r'^compose/$', compose_message, name='compose_message'),
    url(r'^message/(?P<id>[\d]+)/$', show_message, name='show_message'),



)
