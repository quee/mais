# Create your views here.
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from messages.forms import ComposeMessageForm
from messages.models import Message

@login_required
def show_messages(request):
    template = "messages/show_all.html"
    user = request.user # https://docs.djangoproject.com/en/dev/ref/request-response/#django.http.HttpRequest.user
    messages = Message.objects.filter( to_user = user )

    data = {'messages': messages}
    return render_to_response(template, data, context_instance=RequestContext(request))

@login_required
def compose_message(request):
    template = "messages/compose.html"
    user = request.user # https://docs.djangoproject.com/en/dev/ref/request-response/#django.http.HttpRequest.user
    form = ComposeMessageForm()

    if request.POST: # IF USER POST HIS DATA, THEN WE CHECK IF EVERYTHING IS VALID
        form = ComposeMessageForm(request.POST) # pass posted data to the form
        if form.is_valid():
            form.save(from_user = user) # FORM IS VALID, WE SAVE IT
            return redirect(reverse("show_messages"))

    data = {'form': form}
    return render_to_response(template, data, context_instance=RequestContext(request))

def show_message(request,id):
    template = "messages/show_message.html"
    user = request.user # https://docs.djangoproject.com/en/dev/ref/request-response/#django.http.HttpRequest.user
    message = Message.objects.get(id=id,to_user=user)

    data = {'message': message}
    return render_to_response(template, data, context_instance=RequestContext(request))
