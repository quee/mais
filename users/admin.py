# -*- coding: utf-8 -*-
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.utils.html import escape
from django.utils.translation import ugettext
from users.models import Child, Parent, Teacher, BaseUser
from django import forms

class BaseUserAdminForm(forms.ModelForm):

    class Meta:
        model = BaseUser

    def __init__(self, *args, **kwargs):
        super(BaseUserAdminForm, self).__init__(*args, **kwargs)
        self.fields['password'].help_text = "Use '[algo]$[salt]$[hexdigest]' or use the <a href=\"password/\">change password form</a>."


class BaseUserAdmin(admin.ModelAdmin):
    form = BaseUserAdminForm
    change_password_form = AdminPasswordChangeForm
    change_user_password_template = None

    def get_urls(self):
        from django.conf.urls.defaults import patterns
        return patterns('',
            (r'^(\d+)/password/$', self.admin_site.admin_view(self.participant_change_password))
        ) + super(BaseUserAdmin, self).get_urls()

    def participant_change_password(self, request, id):
        if not self.has_change_permission(request):
            raise PermissionDenied
        participant = get_object_or_404(self.model, pk=id)
        if request.method == 'POST':
            form = self.change_password_form(participant, request.POST)
            if form.is_valid():
                new_user = form.save()
                msg = ugettext('Password changed successfully.')
                messages.success(request, msg)
                return HttpResponseRedirect('..')
        else:
            form = self.change_password_form(participant)

        fieldsets = [(None, {'fields': form.base_fields.keys()})]
        adminForm = admin.helpers.AdminForm(form, fieldsets, {})

        return render_to_response(self.change_user_password_template or 'admin/auth/user/change_password.html', {
            'title': ('Change password: %s') % escape(participant.get_full_name()),
            'adminForm': adminForm,
            'form': form,
            'is_popup': '_popup' in request.REQUEST,
            'add': True,
            'change': False,
            'has_delete_permission': False,
            'has_change_permission': True,
            'has_absolute_url': False,
            'opts': self.model._meta,
            'original': participant,
            'save_as': False,
            'show_save': True,
            'root_path': '',
            }, context_instance=RequestContext(request))

admin.site.register(BaseUser, BaseUserAdmin)
admin.site.register(Teacher, BaseUserAdmin)
admin.site.register(Parent, BaseUserAdmin)
admin.site.register(Child, BaseUserAdmin)
