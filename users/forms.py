import random
import string
from django import forms
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from users.models import BaseUser, Teacher, Parent

exclude_list = ('is_active','is_staff','is_superuser','last_login',)

class BaseRegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), label=u"Password")
    repeat_password = forms.CharField(widget=forms.PasswordInput(), label=u"Repeat password")

    class Meta:
        model = BaseUser
        exclude = exclude_list

    def clean(self):
        cleaned_data = self.cleaned_data
        password = cleaned_data.get("password", None)
        email = cleaned_data.get("email", None)

        if BaseUser.objects.filter(email = email).exists():
            raise forms.ValidationError("This email is already taken")


        repeat_password = cleaned_data.get("repeat_password", None)
        if not password or not repeat_password or password != repeat_password:
            raise forms.ValidationError("Passwords do not mach")

        return cleaned_data

    def save(self, commit=True):
        password = self.cleaned_data['password']
        m = super(BaseRegistrationForm, self).save(commit=False)
        if commit:
            m.set_password(password)
            m.save()
        return m


class ParentRegistrationForm(BaseRegistrationForm):

    class Meta:
        model = Parent
        exclude = exclude_list


class RemindPasswordForm(forms.Form):
    email = forms.EmailField(label=("E-mail"),max_length=200)
    email2 = forms.EmailField(label=(u"Repeat e-mail"),max_length=200)



    def clean(self):
        email = self.cleaned_data.get('email',None)
        email2 = self.cleaned_data.get('email2',None)

        if email and email != email2:
            raise forms.ValidationError(_(u"Given email addres are diffrent"))

        return self.cleaned_data

    def send_password(self):
        user = BaseUser.objects.get(email=self.cleaned_data['email'])
        password = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(8))
        user.set_password(password)
        template = "mails/password_remind.html"
        context= {'password': password,}
        message = render_to_string(template,context)
        subject = (u"Remind password")
        mail_list = [user.email,]
            #print mail_list
       # msg = EmailMessage(subject,message, sender, mail_list)
        #msg.content_subtype = "html"  # Main content is now text/html
        #msg.send()

