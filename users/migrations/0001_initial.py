# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BaseUser'
        db.create_table(u'users_baseuser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'users', ['BaseUser'])

        # Adding model 'Teacher'
        db.create_table(u'users_teacher', (
            (u'baseuser_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['users.BaseUser'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'users', ['Teacher'])

        # Adding model 'Child'
        db.create_table(u'users_child', (
            (u'baseuser_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['users.BaseUser'], unique=True, primary_key=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.Group'], null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['Child'])

        # Adding model 'Parent'
        db.create_table(u'users_parent', (
            (u'baseuser_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['users.BaseUser'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'users', ['Parent'])

        # Adding M2M table for field children on 'Parent'
        db.create_table(u'users_parent_children', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('parent', models.ForeignKey(orm[u'users.parent'], null=False)),
            ('child', models.ForeignKey(orm[u'users.child'], null=False))
        ))
        db.create_unique(u'users_parent_children', ['parent_id', 'child_id'])


    def backwards(self, orm):
        # Deleting model 'BaseUser'
        db.delete_table(u'users_baseuser')

        # Deleting model 'Teacher'
        db.delete_table(u'users_teacher')

        # Deleting model 'Child'
        db.delete_table(u'users_child')

        # Deleting model 'Parent'
        db.delete_table(u'users_parent')

        # Removing M2M table for field children on 'Parent'
        db.delete_table('users_parent_children')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'users.baseuser': {
            'Meta': {'object_name': 'BaseUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'users.child': {
            'Meta': {'object_name': 'Child', '_ormbases': [u'users.BaseUser']},
            u'baseuser_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['users.BaseUser']", 'unique': 'True', 'primary_key': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']", 'null': 'True', 'blank': 'True'})
        },
        u'users.parent': {
            'Meta': {'object_name': 'Parent', '_ormbases': [u'users.BaseUser']},
            u'baseuser_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['users.BaseUser']", 'unique': 'True', 'primary_key': 'True'}),
            'children': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['users.Child']", 'symmetrical': 'False'})
        },
        u'users.teacher': {
            'Meta': {'object_name': 'Teacher', '_ormbases': [u'users.BaseUser']},
            u'baseuser_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['users.BaseUser']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['users']