# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'BaseUser.is_active'
        db.add_column(u'users_baseuser', 'is_active',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'BaseUser.is_active'
        db.delete_column(u'users_baseuser', 'is_active')


    models = {
        u'groups.childrengroup': {
            'Meta': {'object_name': 'ChildrenGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'teachers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['users.Teacher']", 'null': 'True', 'blank': 'True'})
        },
        u'users.baseuser': {
            'Meta': {'object_name': 'BaseUser'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'users.child': {
            'Meta': {'object_name': 'Child', '_ormbases': [u'users.BaseUser']},
            u'baseuser_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['users.BaseUser']", 'unique': 'True', 'primary_key': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['groups.ChildrenGroup']", 'null': 'True', 'blank': 'True'})
        },
        u'users.parent': {
            'Meta': {'object_name': 'Parent', '_ormbases': [u'users.BaseUser']},
            u'baseuser_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['users.BaseUser']", 'unique': 'True', 'primary_key': 'True'}),
            'children': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['users.Child']", 'symmetrical': 'False'})
        },
        u'users.teacher': {
            'Meta': {'object_name': 'Teacher', '_ormbases': [u'users.BaseUser']},
            u'baseuser_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['users.BaseUser']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['users']