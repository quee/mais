# -*- coding: utf-8 -*-
import datetime
from django.contrib.auth.models import AbstractBaseUser
from groups.models import ChildrenGroup
from users.managers import UserManager
from django.db import models
from django.utils.translation import ugettext as _

class BaseUser(AbstractBaseUser):
    #username = models.CharField(verbose_name=_("Username"), max_length=100, unique=True)
    first_name = models.CharField(verbose_name=_("First name"), max_length=100)
    last_name = models.CharField(verbose_name=_("Last name"), max_length=100)
    email = models.EmailField(verbose_name=_("Email"), unique=True)

    #admi fields
    is_active = models.BooleanField(verbose_name=_("Is active"), default=False)
    is_staff = models.BooleanField(verbose_name=_("Is staff"), default=False)
    is_superuser = models.BooleanField(verbose_name=_("Is superuser"), default=False)

    objects = UserManager()
    USERNAME_FIELD = 'email'

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    def get_full_name(self):
        # The user is identified by their email address
        return "%s %s" % (self.first_name, self.last_name)

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def get_teacher_instance(self):
        try:
            return Teacher.objects.get(id=self.id)
        except:
            return

    def is_teacher(self):
        if self.get_teacher_instance():
            return True
        return False

    def get_parent_instance(self):
        try:
            return Parent.objects.get(id=self.id)
        except:
            return

    def is_parent(self):
        if self.get_parent_instance():
            return True
        return False

    def get_child_instance(self):
        try:
            return Child.objects.get(id=self.id)
        except:
            return

    def is_child(self):
        if self.get_child_instance():
            return True
        return False


class Teacher(BaseUser):
    pass

    class Meta:
        verbose_name = _("Teacher")
        verbose_name_plural = _("Teachers")

    def get_groups(self):
        return self.childrengroup_set.all()




class Child(BaseUser):
    group = models.ForeignKey(ChildrenGroup, verbose_name=_("Group"), help_text=_("Belongs to group"), null=True, blank=True)


    class Meta:
        verbose_name = _("Child")
        verbose_name_plural = _("Children")

    def is_present_today(self):
        kwargs = {'date':datetime.datetime.now().date() }
        attend = self.attend_set.filter(**kwargs)
        if len(attend) > 0:
            return True
        else:
            return False

    def get_all_presence(self):
        return self.attend_set.all().order_by('-date')

    def get_all_games(self):
        return self.gameanswer_set.all()

    def get_all_grades(self):
        return self.grade_set.all()

class Parent(BaseUser):
    children = models.ManyToManyField(Child, verbose_name=_("Children"), help_text=_("Children of this parent"))

    class Meta:
        verbose_name = _("Parent")
        verbose_name_plural = _("Parents")

    def get_all_children(self):
        return self.children.all()



class Attend(models.Model):
    date = models.DateField(verbose_name="Date", default=datetime.datetime.now())
    child = models.ForeignKey(Child, verbose_name="Child")

    def __unicode__(self):
        return "%s %s" % (self.date, self.child)


GRADES = (
    (":)", ":)"),
    (":(", ":("),
)

class Grade(models.Model):
    grade = models.CharField(verbose_name="Grade", choices=GRADES,max_length=20)
    date = models.DateField(verbose_name="Date", default=datetime.datetime.now())
    child = models.ForeignKey(Child, verbose_name="Child")

    def __unicode__(self):
        return self.grade

