from django.conf.urls import patterns, include, url
from users.forms import ParentRegistrationForm
from users.views import registration, show_profile, set_presence, show_child_profile, add_grade
from django.contrib.auth import views as auth_views
from users.views import widok

urlpatterns = patterns('',
    # Examples:'
    url(r'^profile/$', show_profile, name='show_profile'),
    url(r'^profile/child/(?P<user_id>[\d]+)/$', show_child_profile, name='show_child_profile'),

    url(r'^set_present/(?P<child_id>[\d]+)/$', set_presence, name='set_presence'),
    url(r'^add_grade/(?P<child_id>[\d]+)/(?P<grade>[\d]+)$', add_grade, name='add_grade'),


    url(r'^login/$', auth_views.login, name='auth_login'),

    url(r'^logout/$', auth_views.logout, name='auth_logout'),
    url(r'^registration/$', registration, name='registration'),
    url(r'^registration/parent/$', registration,{'registration_form':ParentRegistrationForm}, name='registration_parent'),

)
