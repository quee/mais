# Create your views here.
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from users.forms import BaseRegistrationForm, RemindPasswordForm
from users.models import Child, BaseUser, Attend, Grade


def home(request):
    """
    View fore home site, it does nothing execpt choosing template
    """
    template = "home.html"
    qwe = 123
    qwe = 1 + 2 +3
    children = Child.objects.all()
    data = {'cos':12,'kolejne':13,'children':children}
    return render_to_response(template, data, context_instance=RequestContext(request))


def widok(request):
    template = "home.html"
    qwe = 123
    data = {'cos':12,'kolejne':13,'kolejne2':qwe}
    return render_to_response(template, data, context_instance=RequestContext(request))


def registration(request,registration_form=BaseRegistrationForm):
    """
    View for registration of users

    @param request: always required in views
    @param registration_form: chosen form, default BaseRegistrationForm
    """
    template = "registration/registration.html"

    form = registration_form()
    if request.POST:
        form = registration_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("home"))

    data = {'form': form}
    return render_to_response(template, data, context_instance=RequestContext(request))


def remind_password(request):
    template = "registration/remind_password.html"
    form = RemindPasswordForm()
    if request.POST:
        form = RemindPasswordForm(request.POST)
        if form.is_valid():
            form.send_password()
            return redirect(reverse("home"))

    data = {'form':form}
    return render_to_response(template, data, context_instance=RequestContext(request))


def show_profile(request, user_id=None):
    try:
        user = BaseUser.objects.get(id=user_id)
    except:
        user = request.user

    template = "profiles/profile.html"
    data = {'user':user}
    return render_to_response(template, data, context_instance=RequestContext(request))

def show_child_profile(request, user_id=None):
    try:
        user = Child.objects.get(id=user_id)
    except:
        user = request.user

    template = "profiles/child_profile.html"
    data = {'child':user}
    return render_to_response(template, data, context_instance=RequestContext(request))


def set_presence(request, child_id):
    child = Child.objects.get(id=child_id)
    a = Attend.objects.create(child=child)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def add_grade(request, child_id, grade):
    child = Child.objects.get(id=child_id)
    if int(grade) == 1:
        grade = ":)"
    else:
        grade = ":("
    a = Grade.objects.create(child=child, grade=grade)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))